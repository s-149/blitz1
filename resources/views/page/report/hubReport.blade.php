 <?php

$noData =1 ;

$data = [
    [
        'no'=> 1,
        'area'=> 'global',
        'jumlahLate'=> count($globalLate),
        'jumlahOnTime'=> count($globalOnTime),
        'jumlahFaktur'=> count($globalOnTime) + count($globalLate),
        'percentLate'=> round(count($globalLate) / (count($globalOnTime) + count($globalLate)) * 100, 2),
        'percentOnTime'=> round(count($globalOnTime) / (count($globalOnTime) + count($globalLate)) * 100, 2),
    ],
    [
        'no'=> 2,
        'area'=> 'JK1',
        'jumlahLate'=> count($jk1Late),
        'jumlahOnTime'=> count($jk1OnTime),
        'jumlahFaktur'=> count($jk1OnTime) + count($jk1Late),
        'percentLate'=> round(count($jk1Late) / (count($jk1OnTime) + count($jk1Late)) * 100, 2),
        'percentOnTime'=> round(count($jk1OnTime) / (count($jk1OnTime) + count($jk1Late)) * 100, 2),
    ],
    [
        'no'=> 3,
        'area'=> 'JK2',
        'jumlahLate'=> count($jk2Late),
        'jumlahOnTime'=> count($jk2OnTime),
        'jumlahFaktur'=> count($jk2OnTime) + count($jk2Late),
        'percentLate'=> round(count($jk2Late) / (count($jk2OnTime) + count($jk2Late)) * 100, 2),
        'percentOnTime'=> round(count($jk2OnTime) / (count($jk2OnTime) + count($jk2Late)) * 100, 2),
    ],
    [
        'no'=> 4,
        'area'=> 'BDG',
        'jumlahLate'=> count($bdgLate),
        'jumlahOnTime'=> count($bdgOnTime),
        'jumlahFaktur'=> count($bdgOnTime) + count($bdgLate),
        'percentLate'=> round(count($bdgLate) / (count($bdgOnTime) + count($bdgLate)) * 100, 2),
        'percentOnTime'=> round(count($bdgOnTime) / (count($bdgOnTime) + count($bdgLate)) * 100, 2),
    ],
    [
        'no'=> 5,
        'area'=> 'BTN',
        'jumlahLate'=> count($btnLate),
        'jumlahOnTime'=> count($btnOnTime),
        'jumlahFaktur'=> count($btnOnTime) + count($btnLate),
        'percentLate'=> round(count($btnLate) / (count($btnOnTime) + count($btnLate)) * 100, 2),
        'percentOnTime'=> round(count($btnOnTime) / (count($btnOnTime) + count($btnLate)) * 100, 2),
    ],
    [
        'no'=> 6,
        'area'=> 'DPK',
        'jumlahLate'=> count($dpkLate),
        'jumlahOnTime'=> count($dpkOnTime),
        'jumlahFaktur'=> count($dpkOnTime) + count($dpkLate),
        'percentLate'=> round(count($dpkLate) / (count($dpkOnTime) + count($dpkLate)) * 100, 2),
        'percentOnTime'=> round(count($dpkOnTime) / (count($dpkOnTime) + count($dpkLate)) * 100, 2),
    ],
    [
        'no'=> 7,
        'area'=> 'MLG',
        'jumlahLate'=> count($mlgLate),
        'jumlahOnTime'=> count($mlgOnTime),
        'jumlahFaktur'=> count($mlgOnTime) + count($mlgLate),
        'percentLate'=> round(count($mlgLate) / (count($mlgOnTime) + count($mlgLate)) * 100, 2),
        'percentOnTime'=> round(count($mlgOnTime) / (count($mlgOnTime) + count($mlgLate)) * 100, 2),
    ],
    [
        'no'=> 8,
        'area'=> 'SMG',
        'jumlahLate'=> count($smgLate),
        'jumlahOnTime'=> count($smgOnTime),
        'jumlahFaktur'=> count($smgOnTime) + count($smgLate),
        'percentLate'=> round(count($smgLate) / (count($smgOnTime) + count($smgLate)) * 100, 2),
        'percentOnTime'=> round(count($smgOnTime) / (count($smgOnTime) + count($smgLate)) * 100, 2),
    ],
    [
        'no'=> 9,
        'area'=> 'SDO',
        'jumlahLate'=> count($sdoLate),
        'jumlahOnTime'=> count($sdoOnTime),
        'jumlahFaktur'=> count($sdoOnTime) + count($sdoLate),
        'percentLate'=> round(count($sdoLate) / (count($sdoOnTime) + count($sdoLate)) * 100, 2),
        'percentOnTime'=> round(count($sdoOnTime) / (count($sdoOnTime) + count($sdoLate)) * 100, 2),
    ],
    [
        'no'=> 10,
        'area'=> 'SD2',
        'jumlahLate'=> count($sd2Late),
        'jumlahOnTime'=> count($sd2OnTime),
        'jumlahFaktur'=> count($sd2OnTime) + count($sd2Late),
        'percentLate'=> round(count($sd2Late) / (count($sd2OnTime) + count($sd2Late)) * 100, 2),
        'percentOnTime'=> round(count($sd2OnTime) / (count($sd2OnTime) + count($sd2Late)) * 100, 2),
    ],
    [
        'no'=> 11,
        'area'=> 'SLO',
        'jumlahLate'=> count($sloLate),
        'jumlahOnTime'=> count($sloOnTime),
        'jumlahFaktur'=> count($sloOnTime) + count($sloLate),
        'percentLate'=> round(count($sloLate) / (count($sloOnTime) + count($sloLate)) * 100, 2),
        'percentOnTime'=> round(count($sloOnTime) / (count($sloOnTime) + count($sloLate)) * 100, 2),
    ],
    [
        'no'=> 12,
        'area'=> 'YOG',
        'jumlahLate'=> count($yogLate),
        'jumlahOnTime'=> count($yogOnTime),
        'jumlahFaktur'=> count($yogOnTime) + count($yogLate),
        'percentLate'=> round(count($yogLate) / (count($yogOnTime) + count($yogLate)) * 100, 2),
        'percentOnTime'=> round(count($yogOnTime) / (count($yogOnTime) + count($yogLate)) * 100, 2),
    ]

];

{{--  @dd($days)  --}}

 ?>

 @extends('layout.main')

 @section('container')

                <section id="main-content">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="bootstrap-data-table-panel">
                                    <div class="table-responsive">
                                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Area</th>
                                                    <th>Jumlah Late</th>
                                                    <th>Jumlah On Time</th>
                                                    <th>Jumlah Faktur</th>
                                                    <th>Percent Late</th>
                                                    <th>Percent On Time</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($data as $data)

                                                    <tr>
                                                        <td>{{ $data['no'] }}</td>
                                                        <td>{{ $data['area'] }}</td>
                                                        <td>{{ $data['jumlahLate'] }}</td>
                                                        <td>{{ $data['jumlahOnTime'] }}</td>
                                                        <td>{{ $data['jumlahFaktur'] }}</td>
                                                        <td>{{ $data['percentLate'] }}</td>
                                                        <td>{{ $data['percentOnTime'] }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>

@endsection
