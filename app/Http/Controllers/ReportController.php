<?php

namespace App\Http\Controllers;

use App\Models\Report;
use App\Http\Requests\StoreReportRequest;
use App\Http\Requests\UpdateReportRequest;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('page.report.index', [
                        'title' => 'Read',
                        'faktur' => Report::all()
                    ]);
    }

    public function hubReport()
    {
        return view('page.report.hubReport', [
            'title' => 'Hub Report',

            // Report Per Bulan

            // global faktur
            'globalLate' => Report::where('sla', 'LATE')->get(),
            'globalOnTime' => Report::where('sla', 'ON TIME')->get(),
            'jumlahFaktur' => Report::all(),

            // jk1
            'jk1Late' => Report::where('sla', 'LATE')->where('hub', 'JK1')->get(),
            'jk1OnTime' => Report::where('sla', 'ON TIME')->where('hub', 'JK1')->get(),

            // jk2
            'jk2Late' => Report::where('sla', 'LATE')->where('hub', 'JK2')->get(),
            'jk2OnTime' => Report::where('sla', 'ON TIME')->where('hub', 'JK2')->get(),

            // bdg
            'bdgLate' => Report::where('sla', 'LATE')->where('hub', 'BDG')->get(),
            'bdgOnTime' => Report::where('sla', 'ON TIME')->where('hub', 'BDG')->get(),

            // btn
            'btnLate' => Report::where('sla', 'LATE')->where('hub', 'BTN')->get(),
            'btnOnTime' => Report::where('sla', 'ON TIME')->where('hub', 'BTN')->get(),

            // dpk
            'dpkLate' => Report::where('sla', 'LATE')->where('hub', 'DPK')->get(),
            'dpkOnTime' => Report::where('sla', 'ON TIME')->where('hub', 'DPK')->get(),

            // mlg
            'mlgLate' => Report::where('sla', 'LATE')->where('hub', 'MLG')->get(),
            'mlgOnTime' => Report::where('sla', 'ON TIME')->where('hub', 'MLG')->get(),

            // smg
            'smgLate' => Report::where('sla', 'LATE')->where('hub', 'SMG')->get(),
            'smgOnTime' => Report::where('sla', 'ON TIME')->where('hub', 'SMG')->get(),

            // sdo
            'sdoLate' => Report::where('sla', 'LATE')->where('hub', 'SDO')->get(),
            'sdoOnTime' => Report::where('sla', 'ON TIME')->where('hub', 'SDO')->get(),

            // sd2
            'sd2Late' => Report::where('sla', 'LATE')->where('hub', 'SD2')->get(),
            'sd2OnTime' => Report::where('sla', 'ON TIME')->where('hub', 'SD2')->get(),

            // slo
            'sloLate' => Report::where('sla', 'LATE')->where('hub', 'SLO')->get(),
            'sloOnTime' => Report::where('sla', 'ON TIME')->where('hub', 'SLO')->get(),

            // yog
            'yogLate' => Report::where('sla', 'LATE')->where('hub', 'YOG')->get(),
            'yogOnTime' => Report::where('sla', 'ON TIME')->where('hub', 'YOG')->get(),


        ]);
    }

    public function dailyReport()
    {
        return view('page.report.dailyReport', [
            'title' => 'Daily Report',

            // Report Per Hari

            // days
            'days' => [

                [
                    'tanggal' => '01',
                    'late' => Report::where('sla', 'LATE')->where('d', '01')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '01')->get()
                ],

                [
                    'tanggal' => '02',
                    'late' => Report::where('sla', 'LATE')->where('d', '02')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '02')->get()
                ],

                [
                    'tanggal' => '03',
                    'late' => Report::where('sla', 'LATE')->where('d', '03')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '03')->get()
                ],

                [
                    'tanggal' => '04',
                    'late' => Report::where('sla', 'LATE')->where('d', '04')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '04')->get()
                ],

                [
                    'tanggal' => '05',
                    'late' => Report::where('sla', 'LATE')->where('d', '05')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '05')->get()
                ],

                [
                    'tanggal' => '06',
                    'late' => Report::where('sla', 'LATE')->where('d', '06')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '06')->get()
                ],

                [
                    'tanggal' => '07',
                    'late' => Report::where('sla', 'LATE')->where('d', '07')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '07')->get()
                ],

                [
                    'tanggal' => '08',
                    'late' => Report::where('sla', 'LATE')->where('d', '08')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '08')->get()
                ],

                [
                    'tanggal' => '09',
                    'late' => Report::where('sla', 'LATE')->where('d', '09')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '09')->get()
                ],

                [
                    'tanggal' => '10',
                    'late' => Report::where('sla', 'LATE')->where('d', '10')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '10')->get()
                ],

                [
                    'tanggal' => '11',
                    'late' => Report::where('sla', 'LATE')->where('d', '11')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '11')->get()
                ],

                [
                    'tanggal' => '12',
                    'late' => Report::where('sla', 'LATE')->where('d', '12')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '12')->get()
                ],

                [
                    'tanggal' => '13',
                    'late' => Report::where('sla', 'LATE')->where('d', '13')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '13')->get()
                ],

                [
                    'tanggal' => '14',
                    'late' => Report::where('sla', 'LATE')->where('d', '14')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '14')->get()
                ],

                [
                    'tanggal' => '15',
                    'late' => Report::where('sla', 'LATE')->where('d', '15')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '15')->get()
                ],

                [
                    'tanggal' => '16',
                    'late' => Report::where('sla', 'LATE')->where('d', '16')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '16')->get()
                ],

                [
                    'tanggal' => '17',
                    'late' => Report::where('sla', 'LATE')->where('d', '17')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '17')->get()
                ],

                [
                    'tanggal' => '18',
                    'late' => Report::where('sla', 'LATE')->where('d', '18')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '18')->get()
                ],

                [
                    'tanggal' => '19',
                    'late' => Report::where('sla', 'LATE')->where('d', '19')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '19')->get()
                ],

                [
                    'tanggal' => '20',
                    'late' => Report::where('sla', 'LATE')->where('d', '20')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '20')->get()
                ],

                [
                    'tanggal' => '21',
                    'late' => Report::where('sla', 'LATE')->where('d', '21')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '21')->get()
                ],

                [
                    'tanggal' => '22',
                    'late' => Report::where('sla', 'LATE')->where('d', '22')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '22')->get()
                ],

                [
                    'tanggal' => '23',
                    'late' => Report::where('sla', 'LATE')->where('d', '23')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '23')->get()
                ],

                [
                    'tanggal' => '24',
                    'late' => Report::where('sla', 'LATE')->where('d', '24')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '24')->get()
                ],

                [
                    'tanggal' => '25',
                    'late' => Report::where('sla', 'LATE')->where('d', '25')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '25')->get()
                ],

                [
                    'tanggal' => '26',
                    'late' => Report::where('sla', 'LATE')->where('d', '26')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '26')->get()
                ],

                [
                    'tanggal' => '27',
                    'late' => Report::where('sla', 'LATE')->where('d', '27')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '27')->get()
                ],

                [
                    'tanggal' => '28',
                    'late' => Report::where('sla', 'LATE')->where('d', '28')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '28')->get()
                ],

                [
                    'tanggal' => '29',
                    'late' => Report::where('sla', 'LATE')->where('d', '29')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '29')->get()
                ],

                [
                    'tanggal' => '30',
                    'late' => Report::where('sla', 'LATE')->where('d', '30')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '30')->get()
                ],

                [
                    'tanggal' => '31',
                    'late' => Report::where('sla', 'LATE')->where('d', '31')->get(),
                    'onTime' => Report::where('sla', 'ON TIME')->where('d', '31')->get()
                ]
            ],



            // 'daysOnTime' => Report::where('sla', 'ON TIME')->where('d', 'days')->get(),

            // dd($Late);




        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreReportRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Report $report)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Report $report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateReportRequest $request, Report $report)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Report $report)
    {
        //
    }
}
