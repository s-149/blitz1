 <?php $a =1 ; ?>

 @extends('layout.main')

 @section('container')

                <section id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="col-lg-3">
                                    <a href="/taskstore" class="btn btn-primary">Create</a>
                                </div>
                                <br>
                                <div class="bootstrap-data-table-panel">
                                    <div class="table-responsive">
                                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Flow</th>
                                                    <th>Hub Id</th>
                                                    <th>Start Time</th>
                                                    <th>End Time</th>
                                                    <th>Created From</th>
                                                    <th>Nama Klien</th>
                                                    <th>Alamat</th>
                                                    <th>Koordinat</th>
                                                    <th>Hampers Type</th>
                                                    <th>Telefon</th>
                                                    {{--  <th><center>Action Detail</center></th>
                                                    <th><center>Action Update</center></th>
                                                    <th><center>Action Delete</center></th> --}}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($task as $task)
                                                <tr>
                                                    <td>{{ $a++ }}</td>
                                                    <td>{{ $task->flow }}</td>
                                                    <td>{{ $task->hubId }}</td>
                                                    <td>{{ $task->startTime }}</td>
                                                    <td>{{ $task->endTime }}</td>
                                                    <td>{{ $task->createdFrom }}</td>
                                                    <td>{{ $task->namaKlien }}</td>
                                                    <td>{{ $task->alamat }}</td>
                                                    <td>{{ $task->koordinat }}</td>
                                                    <td>{{ $task->hampersType }}</td>
                                                    <td>{{ $task->telefon }}</td>
                                                    {{--  <td>
                                                        <a href="task/{{$task->id}}" class="btn btn-success">Detail</a>
                                                    </td>
                                                    <td>
                                                        <a href="taskupdate/{{$task->id}}" class="btn btn-warning">Update</a>
                                                    </td>
                                                    <td>
                                                        <a href="taskdestroy/{{$task->id}}" class="btn btn-danger" onclick="return confirm('yakin ingin menghapus data ini.?')">Delete</a>
                                                    </td>--}}
                                                </tr>
                                                @endForeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

@endsection
