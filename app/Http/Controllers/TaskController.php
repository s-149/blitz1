<?php

namespace App\Http\Controllers;



use App\Models\Task;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Http\Resources\TaskResource;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

// salah satu fungsinya untuk menghapus gambar dari server
use Illuminate\Support\Facades\Storage;

// membuat manual validator untuk store
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        echo " hello world";
        return view('page.task.index', [
            'title' => 'All',
            'task' => Task::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('page.task.store', [
            'title' => 'Create New',

        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // dd($request);
        $validator = Validator::make($request->all(), [
            'flow' => 'required',
            'hubId' => 'required',
            'startTime' => 'required',
            'endTime' => 'required',
            'createdFrom' => 'required',
            'namaKlien' => 'required',
            'alamat' => 'required',
            'koordinat' => 'required',
            'hampersType' => 'required',
            'telefon' => 'required',
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //create post
        $task = task::create([
            'flow' => $request->flow,
            'hubId' => $request->hubId,
            'startTime' => $request->startTime,
            'endTime' => $request->endTime,
            'createdFrom' => $request->createdFrom,
            'namaKlien' => $request->namaKlien,
            'alamat' => $request->alamat,
            'koordinat' => $request->koordinat,
            'hampersType' => $request->hampersType,
            'telefon' => $request->telefon,
        ]);

        $yourPublicToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI2MmNmZWY2NDQ4NGMxZTcwMTU0YjQ5YjIiLCJqdGkiOiIwYWY3NGYyNzQ4ZmIxY2JjNzk2NTUxZjBhYWU1MDE5MDJhMTA5MTk2YzdmOWFhNWJhZjdiZTZiODc3NTE5ZTgwN2FmZDAwZWQ0MzBlNTk2ZCIsImlhdCI6MTY4NDg2MjY0MCwibmJmIjoxNjg0ODYyNjQwLCJleHAiOjQ4NDA1MzYyNDAsInN1YiI6IjY0NTliNDJlOWNhYWI2YTkyOTBjMjY5MiIsInNjb3BlcyI6W119.c5RW-Zj5cee-P-iNKO200l_dduRTxkyq_xjQHsUZ6SVovKZYsztbbah5sFV-5llyY0ig8-S_JRMh6aVMHiyxrctnlC9TndHOcVCQx6p_OQnnL5FABfpcqOJIqaswGgweNJspgeq7kMOq3KTaskDhoYIlBXIrkrpUh4eDjjkpyOGkZjwB2QecV90cP-iq_iMHc_-bGbvk-nJpHMrvUBwx8uT7iZSrLIvmjkc4VpK2tLdZU5ntnshuKBILgOgStf2P5o2jj5OUt4tkmvEaBoaCFOkdnAkoMwGXelTslyl5ALmCYmi8_fn-vi98yzjPhGY-VZ8O1_lmhaL0p_8m734s4rtUTq__jenoamEo96hNcmGJ0byXzd13MpzkJinlDpstEw5K2ndb6HUfGp7uGWObogXj8VTpb0_pNZgq1krIav3NUJvIABvILVDzuW1XYSLvHuWTpjMlAF3NkD3jMOawt9k74IdL8yePd4TcbjsHeyyyBfxqqGp7ZGDLo4fFl8iYIh3OUG9PzuKIAnZpPcWSUt6qhsk8Kw7Gj-heyNMViWTLJh-NNgHlyDxban3TqtAd9qAKQ7NJoPApOqOzMlF83UKm_1ewqMNuUEYvQYjR8Gw7oYBWyH7xRPlE-O0FzvIXs8Hcyxh9EU49KZuP6xq7W9CmiXuxyyGiNRArBo3yGfk';

        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.$yourPublicToken,
        ])->post('https://apiweb.mile.app/api/v3/task', [
            'task' => $request->all(),
        ]);
        if ($response->successful()) {
            $data = $response->json();
            // Lakukan sesuatu dengan data yang diterima
            return response()->json(['success' => 'Berhasil membuat data melalui API', 'data' => $data], 200);
            // return redirect('/task');
        } else {
            // return response()->json(['error' => 'Gagal membuat data melalui API'], 500);
            return redirect('/task');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateTaskRequest $request, Task $task)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        echo "hello";
        // task::destroy($id);

        // return redirect('/task');
    }


    // Route Rest Api

    public function postToPublicApi(Request $request)
    {
        $client = new Client();

        try {
            $response = $client->post('https://apiweb.mile.app/api/v3/task', [
                'headers' => [
                    'Authorization' => 'Bearer {eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI2MmNmZWY2NDQ4NGMxZTcwMTU0YjQ5YjIiLCJqdGkiOiJjODU2ZTIyYzA5ZWI4NGFjNmQxNTQwNzlhYzI5N2Q4OTYyMWUxODc3ZGU1MTA1MDFiZjdkYzA2NjY2Y2JiZTQwMzgxODg4M2RjZTU1MjY4OCIsImlhdCI6MTY4NDc3NDA3MSwibmJmIjoxNjg0Nzc0MDcxLCJleHAiOjQ4NDA0NDc2NzEsInN1YiI6IjY0NTliNDJlOWNhYWI2YTkyOTBjMjY5MiIsInNjb3BlcyI6W119.IdUhWYqieeNkFyF5EAcLQoaQZXtmDiqQiGZgSboBZlX5ccKhdsdK_9HPM8UH4Kfz3y4W-wr_at4HQZjV0Wt-PyVSaG-Xg810RKhnBmWPIzjPANoywvTJiRw6_7ujTxMIQcNcZMxkXB1tvA_T8I731_f1Vf1WBlgzNUfJ0TwySxZWIIY7y3021RUJAxxnVEGb3nBBJ-Y_tjQ2N2fS5mkmG4zSHbFqT_k-_-AmY-fsmMXHeXwxUONN3SP5k_jnnc9KGpSwmnXtGe4yIm38EHZIPOcRm9aalq2d5wiXHTem9VLwgWvLrjClBXfXYK96AeBhUuqTsPFSf-ZlGSlBikRdFUMLuADtWTEXBK1St64Zg3-Pu_9XeY_n8k1GsWYgMJgwOBl_ixuXd9DV86ovkNmzV2dS_5D2pI-MVsA2jgWuodSwfoldbY80dvX6CKWoFF25E5LFRmFlp3ZCE00zEu0aIpRH1jPinURbdxTwH2kRWG5Iq3dg8xv4B1x_6MNTraUS29fozqPLapoEe6U4lF5aVezXECc30x8ogHbys5r7Zgf_1-ikuUOKtoCQPnBD0H4dzvlbgkC0_ajTrSayfj3W-Nk6wwKN8bz0jPEOq7-mFTRY52dnyRzKIJsr1bUCDLAmvxuO8zhZxnWLvhpRDQPvEYoE_03mrscMKt4Ohw7Tp8k}',
                ],
                'form_params' => [
                    'flow' => $request->flow,
                    'hubId' => $request->hubId,
                    'startTime' => $request->startTime,
                    'endTime' => $request->endTime,
                    'createdFrom' => $request->createdFrom,
                    'namaKlien' => $request->namaKlien,
                    'alamat' => $request->alamat,
                    'koordinat' => $request->koordinat,
                    'hampersType' => $request->hampersType,
                    'telefon' => $request->telefon,
                ]
            ]);

            // Handle successful response
            $statusCode = $response->getStatusCode();
            $responseData = $response->getBody()->getContents();

            // Do something with the response data

            return "Request successful";
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $statusCode = $e->getResponse()->getStatusCode();
                $errorResponse = $e->getResponse()->getBody()->getContents();

                // Log or handle the error response

                return "Error occurred: " . $e->getMessage();
            } else {
                return "Error occurred: " . $e->getMessage();
            }
        }
    }


    // get All Task

    public function getAllTask()
    {
        $yourPublicToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI2MmNmZWY2NDQ4NGMxZTcwMTU0YjQ5YjIiLCJqdGkiOiJjZTRjMmNhN2FmMzRkMWVhYjc4ZDE5ZjA1ZWUxODJlZTYyMTE2MjQxYzg3OTg3YjZkYWZkMDBlMmMxZWQyNWRiMDZmODc1NDA1ZjdiNDI4NyIsImlhdCI6MTY4NTAyNjg2NCwibmJmIjoxNjg1MDI2ODY0LCJleHAiOjQ4NDA3MDA0NjQsInN1YiI6IjY0NTliNDJlOWNhYWI2YTkyOTBjMjY5MiIsInNjb3BlcyI6W119.KaNYYP7GOKknc3F_NAKgmgAXHrmksv4G9POLyR4mk5qBDXzc2UIPplbLARG5ZOTl-7LZYnk80_g0fioq-x0v9UZ8xRLgN3DYU77wU7HoQTgLZDJ5IKK-3Ux1-4S9korKlABknYwCJ-Lck3iFQywHd3BzWb1aZ0j2KQ6yQ_Rx9B_syAQXw2843HWwFjkpD6Ao1ZYnz7Om5_c-psqVx9oaonnGmLCzOInIDok2cGbeGKio1QbWqXp-CGPw5ffC_jrmndjg2b23wyFehC98Kk88W_d-u7nnhD3Vx72_ePT__pJwi0BH1ur5ePcY49VFfj8ZZoOKcJy-R8Nh5AxMGy84MkZfbkqSPoTSx9BeJQLkcHfH1P82O2GUqURqkGKzOSL1R1kc2oX2mznC1kMBq62r_GBMHt0BvxnNz9kIULUHYmTYwtpedW82YYy1vfoNQVQcK5wXxKpq5uZtGf2QT0mFR87ima1qWCJY2UKfD9AMfrBDkbtDQFswVv57NwpLj__yZ1gI0v7om3mibZy_vejSPG8c3b5ZF1c8KaJpVbrIsg2cTRBFH6m_ob6MkwkTZzDuqA3kHTejhGEmxsohcRc4UhBT2PSLQm6GW50Z1v7tejcE9soal2E0UVQCU2ggXtlRs4bYphjJaCIprHt3Ed4Y4iKlD9yYiuDuEyOD5gTEbos';

        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.$yourPublicToken,
        ])->get('https://apiweb.mile.app/api/v3/tasks?flow=Merapi&limit=2000');

        // dd($response);

        if ($response->successful()) {
            $data = $response->json();
            // Lakukan sesuatu dengan data yang diterima
            // return response()->json(['success' => 'Berhasil mengambil data dari API', 'data' => $data], 200);
            dd($data);
        } else {
            return response()->json(['error' => 'Gagal mengambil data dari API'], 500);
        }
    }

    // Post Task
    public function postTask(Request $request)
    {
        $yourPublicToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI2MmNmZWY2NDQ4NGMxZTcwMTU0YjQ5YjIiLCJqdGkiOiJjZTRjMmNhN2FmMzRkMWVhYjc4ZDE5ZjA1ZWUxODJlZTYyMTE2MjQxYzg3OTg3YjZkYWZkMDBlMmMxZWQyNWRiMDZmODc1NDA1ZjdiNDI4NyIsImlhdCI6MTY4NTAyNjg2NCwibmJmIjoxNjg1MDI2ODY0LCJleHAiOjQ4NDA3MDA0NjQsInN1YiI6IjY0NTliNDJlOWNhYWI2YTkyOTBjMjY5MiIsInNjb3BlcyI6W119.KaNYYP7GOKknc3F_NAKgmgAXHrmksv4G9POLyR4mk5qBDXzc2UIPplbLARG5ZOTl-7LZYnk80_g0fioq-x0v9UZ8xRLgN3DYU77wU7HoQTgLZDJ5IKK-3Ux1-4S9korKlABknYwCJ-Lck3iFQywHd3BzWb1aZ0j2KQ6yQ_Rx9B_syAQXw2843HWwFjkpD6Ao1ZYnz7Om5_c-psqVx9oaonnGmLCzOInIDok2cGbeGKio1QbWqXp-CGPw5ffC_jrmndjg2b23wyFehC98Kk88W_d-u7nnhD3Vx72_ePT__pJwi0BH1ur5ePcY49VFfj8ZZoOKcJy-R8Nh5AxMGy84MkZfbkqSPoTSx9BeJQLkcHfH1P82O2GUqURqkGKzOSL1R1kc2oX2mznC1kMBq62r_GBMHt0BvxnNz9kIULUHYmTYwtpedW82YYy1vfoNQVQcK5wXxKpq5uZtGf2QT0mFR87ima1qWCJY2UKfD9AMfrBDkbtDQFswVv57NwpLj__yZ1gI0v7om3mibZy_vejSPG8c3b5ZF1c8KaJpVbrIsg2cTRBFH6m_ob6MkwkTZzDuqA3kHTejhGEmxsohcRc4UhBT2PSLQm6GW50Z1v7tejcE9soal2E0UVQCU2ggXtlRs4bYphjJaCIprHt3Ed4Y4iKlD9yYiuDuEyOD5gTEbos';
        https://apiweb.mile.app/api/v3/task
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.$yourPublicToken,
        ])->post('https://apiweb.mile.app/api/v3/task', [
            'data' => $request->all(),[
            'flow' => $request->flow,
            'hubId' => $request->hubId,
            'startTime' => $request->startTime,
            'endTime' => $request->endTime,
            'createdFrom' => $request->createdFrom,
            'namaKlien' => $request->namaKlien,
            'alamat' => $request->alamat,
            'koordinat' => $request->koordinat,
            'hampersType' => $request->hampersType,
            'telefon' => $request->telefon,
            ]
        ]);

    //     $response = Http::post('https://apiweb.mile.app/api/v3/tasks/task', [
    //         'data' => $request->all(),
    //     ]);


    // if ($response->successful()) {
    //     // Permintaan berhasil
    //     $responseData = $response->json();
    //     // Lakukan sesuatu dengan data yang diterima
    // } else {
    //     // Permintaan gagal
    //     $errorCode = $response->status();
    //     $errorMessage = $response->body();
    //     $responseData = $response->json();
    // }

        // dd($request);

        if ($response->successful()) {
            $data = $response->json();
            // Lakukan sesuatu dengan data yang diterima
            return response()->json(['success' => 'Berhasil membuat data melalui API', 'data' => $data], 200);
        } else {
            return response()->json(['error' => 'Gagal membuat data melalui API'], 500);
        }
    }


}


// namespace App\Http\Controllers;

// use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Http;
// use App\Http\Resources\TaskResource;
// use App\Models\Task;
// use GuzzleHttp\Client;

// // salah satu fungsinya untuk menghapus gambar dari server
// use Illuminate\Support\Facades\Storage;

// // membuat manual validator untuk store
// use Illuminate\Support\Facades\Validator;

// class TaskController extends Controller
// {
//     public function index()
//     {
//         return view('page.task.index', [
//             'title' => 'All',
//             'task' => Task::all()
//         ]);
//     }
//     /**
//      * Display a listing of the resource.
//      */
//     public function getAllTask()
//     {
//         $yourPublicToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI2MmNmZWY2NDQ4NGMxZTcwMTU0YjQ5YjIiLCJqdGkiOiJjODU2ZTIyYzA5ZWI4NGFjNmQxNTQwNzlhYzI5N2Q4OTYyMWUxODc3ZGU1MTA1MDFiZjdkYzA2NjY2Y2JiZTQwMzgxODg4M2RjZTU1MjY4OCIsImlhdCI6MTY4NDc3NDA3MSwibmJmIjoxNjg0Nzc0MDcxLCJleHAiOjQ4NDA0NDc2NzEsInN1YiI6IjY0NTliNDJlOWNhYWI2YTkyOTBjMjY5MiIsInNjb3BlcyI6W119.IdUhWYqieeNkFyF5EAcLQoaQZXtmDiqQiGZgSboBZlX5ccKhdsdK_9HPM8UH4Kfz3y4W-wr_at4HQZjV0Wt-PyVSaG-Xg810RKhnBmWPIzjPANoywvTJiRw6_7ujTxMIQcNcZMxkXB1tvA_T8I731_f1Vf1WBlgzNUfJ0TwySxZWIIY7y3021RUJAxxnVEGb3nBBJ-Y_tjQ2N2fS5mkmG4zSHbFqT_k-_-AmY-fsmMXHeXwxUONN3SP5k_jnnc9KGpSwmnXtGe4yIm38EHZIPOcRm9aalq2d5wiXHTem9VLwgWvLrjClBXfXYK96AeBhUuqTsPFSf-ZlGSlBikRdFUMLuADtWTEXBK1St64Zg3-Pu_9XeY_n8k1GsWYgMJgwOBl_ixuXd9DV86ovkNmzV2dS_5D2pI-MVsA2jgWuodSwfoldbY80dvX6CKWoFF25E5LFRmFlp3ZCE00zEu0aIpRH1jPinURbdxTwH2kRWG5Iq3dg8xv4B1x_6MNTraUS29fozqPLapoEe6U4lF5aVezXECc30x8ogHbys5r7Zgf_1-ikuUOKtoCQPnBD0H4dzvlbgkC0_ajTrSayfj3W-Nk6wwKN8bz0jPEOq7-mFTRY52dnyRzKIJsr1bUCDLAmvxuO8zhZxnWLvhpRDQPvEYoE_03mrscMKt4Ohw7Tp8k';

//         $response = Http::withHeaders([
//             'Authorization' => 'Bearer '.$yourPublicToken,
//         ])->get('https://apiweb.mile.app/api/v3/tasks');

//         // dd($response);

//         if ($response->successful()) {
//             $data = $response->json();
//             // Lakukan sesuatu dengan data yang diterima
//             return response()->json(['success' => 'Berhasil mengambil data dari API', 'data' => $data], 200);
//             // dd($data);
//         } else {
//             return response()->json(['error' => 'Gagal mengambil data dari API'], 500);
//         }
//     }

//     /**
//      * Show the form for creating a new resource.
//      */
//     public function create()
//     {
//         //
//     }

//     /**
//      * Store a newly created resource in storage.
//      */

//     public function postTask(Request $request)
//     {
//         // dd($request);
//         $validator = Validator::make($request->all(), [
//             'flow' => 'required',
//             'hubId' => 'required',
//             'startTime' => 'required',
//             'endTime' => 'required',
//             'createdFrom' => 'required',
//             'namaKlien' => 'required',
//             'alamat' => 'required',
//             'koordinat' => 'required',
//             'hampersType' => 'required',
//             'telefon' => 'required',
//         ]);

//         //check if validation fails
//         if ($validator->fails()) {
//             return response()->json($validator->errors(), 422);
//         }

//         //upload image
//         // $image = $request->file('image');
//         // $image->storeAs('public/posts', $image->hashName());

//         //create post
//         $order = Order::create([
//             'flow' => $request->flow,
//             'hubId' => $request->hubId,
//             'startTime' => $request->startTime,
//             'endTime' => $request->endTime,
//             'createdFrom' => $request->createdFrom,
//             'namaKlien' => $request->namaKlien,
//             'alamat' => $request->alamat,
//             'koordinat' => $request->koordinat,
//             'hampersType' => $request->hampersType,
//             'telefon' => $request->telefon,
//         ]);

//         //return response
//         // return new OrderResource(true, 'Data Post Berhasil Ditambahkan!', $order);


//         $yourPublicToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI2MmNmZWY2NDQ4NGMxZTcwMTU0YjQ5YjIiLCJqdGkiOiIwYWY3NGYyNzQ4ZmIxY2JjNzk2NTUxZjBhYWU1MDE5MDJhMTA5MTk2YzdmOWFhNWJhZjdiZTZiODc3NTE5ZTgwN2FmZDAwZWQ0MzBlNTk2ZCIsImlhdCI6MTY4NDg2MjY0MCwibmJmIjoxNjg0ODYyNjQwLCJleHAiOjQ4NDA1MzYyNDAsInN1YiI6IjY0NTliNDJlOWNhYWI2YTkyOTBjMjY5MiIsInNjb3BlcyI6W119.c5RW-Zj5cee-P-iNKO200l_dduRTxkyq_xjQHsUZ6SVovKZYsztbbah5sFV-5llyY0ig8-S_JRMh6aVMHiyxrctnlC9TndHOcVCQx6p_OQnnL5FABfpcqOJIqaswGgweNJspgeq7kMOq3KTaskDhoYIlBXIrkrpUh4eDjjkpyOGkZjwB2QecV90cP-iq_iMHc_-bGbvk-nJpHMrvUBwx8uT7iZSrLIvmjkc4VpK2tLdZU5ntnshuKBILgOgStf2P5o2jj5OUt4tkmvEaBoaCFOkdnAkoMwGXelTslyl5ALmCYmi8_fn-vi98yzjPhGY-VZ8O1_lmhaL0p_8m734s4rtUTq__jenoamEo96hNcmGJ0byXzd13MpzkJinlDpstEw5K2ndb6HUfGp7uGWObogXj8VTpb0_pNZgq1krIav3NUJvIABvILVDzuW1XYSLvHuWTpjMlAF3NkD3jMOawt9k74IdL8yePd4TcbjsHeyyyBfxqqGp7ZGDLo4fFl8iYIh3OUG9PzuKIAnZpPcWSUt6qhsk8Kw7Gj-heyNMViWTLJh-NNgHlyDxban3TqtAd9qAKQ7NJoPApOqOzMlF83UKm_1ewqMNuUEYvQYjR8Gw7oYBWyH7xRPlE-O0FzvIXs8Hcyxh9EU49KZuP6xq7W9CmiXuxyyGiNRArBo3yGfk';

//         $response = Http::withHeaders([
//             'Authorization' => 'Bearer '.$yourPublicToken,
//         ])->post('https://apiweb.mile.app/api/v3/tasks/task', [
//             'data' => $request->all(),[
//             'flow' => $request->flow,
//             'hubId' => $request->hubId,
//             'startTime' => $request->startTime,
//             'endTime' => $request->endTime,
//             'createdFrom' => $request->createdFrom,
//             'namaKlien' => $request->namaKlien,
//             'alamat' => $request->alamat,
//             'koordinat' => $request->koordinat,
//             'hampersType' => $request->hampersType,
//             'telefon' => $request->telefon,
//             ]
//         ]);

//     //     $response = Http::post('https://apiweb.mile.app/api/v3/tasks/task', [
//     //         'data' => $request->all(),
//     //     ]);


//     // if ($response->successful()) {
//     //     // Permintaan berhasil
//     //     $responseData = $response->json();
//     //     // Lakukan sesuatu dengan data yang diterima
//     // } else {
//     //     // Permintaan gagal
//     //     $errorCode = $response->status();
//     //     $errorMessage = $response->body();
//     //     $responseData = $response->json();
//     // }

//         // dd($request);

//         if ($response->successful()) {
//             $data = $response->json();
//             // Lakukan sesuatu dengan data yang diterima
//             return response()->json(['success' => 'Berhasil membuat data melalui API', 'data' => $data], 200);
//         } else {
//             return response()->json(['error' => 'Gagal membuat data melalui API'], 500);
//         }
//     }

//     // public function store(Request $request)
//     // {

//     //     // dd($request);
//     //     $yourPublicToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI2MmNmZWY2NDQ4NGMxZTcwMTU0YjQ5YjIiLCJqdGkiOiI0ZGYyZGUzNzAyNTA3NzViZWMyZDgxNzMxNGMyZWU2NjBmZDMzNDQ0ZTNhNDU0MDc0Mjg2ZTJiMTUxNGYyMjY0Njk3MDgxNDkyMDVkMjQzOCIsImlhdCI6MTY4NDc4MDA0NSwibmJmIjoxNjg0NzgwMDQ1LCJleHAiOjQ4NDA0NTM2NDUsInN1YiI6IjY0NTliNDJlOWNhYWI2YTkyOTBjMjY5MiIsInNjb3BlcyI6W119.QM0kgLOHLvoZnuwSsQuGBSsJrlYVv4P6FPk7SDwzHLxHaCW1Q_M-ESf74SU81eCZHByRD7XHYsLUyTTVvu5PweFogMgh0lNqzXYaEziYZkBEK4Fxty_t96WWA-xPitzL0dkxImvHZiSUucVknE_1OaGoMBmYF2LS47RFKNdoiQ0lL-zJa6aSCwmAdfYDqX4TauCTdXWE92U7zuvxwwMISRUX6MCFrgKjWqRljrSGbAj492M-avCJp0tj7R3mW8x8tTiUtYIB_mZVcKQySvKwnASWP0E7oG4Dl4v3qyUnPwv6WXqeD_tm78Q51MB0NQ9--IudiZ_wIDi279OL6hkHCJJhmQi_x9_QrwaewGx7cF4CwJSyOlviK991RemFSoJRZ6g5sNhFd6wSDHJ_UukPuwzDOSIgrRCLFlbtX4Art2ZkrsWz9x989h6Mre9F6JxHqnB2Xfe4xLkCaV5s--zCnkBZdis1o8EvGwPZLL5D5wujJNd4_Deb7gqyChKQz2H2Zo0Xsu9o8n_2y8F8vSU4VvhDLDrfUg32N59uAUS6rc6efdzrCKByVqPt5fiYRKEm_jvNVV-FHkUM-XoMPp0QDtdlYQfulh2v9DLqfYuN1RFITntei111_07LgGKXEKLNDqjbjNSHQfnvUtkT7IqmGaGXZ318Dpd5YM6i7RAgKvw';

//     //     $response = Http::withHeaders([
//     //         'Authorization' => 'Bearer '.$yourPublicToken,
//     //     ])->POST('https://apiweb.mile.app/api/v3/task', [
//     //         'data' => $request->all(),
//     //     ]);

//     //     if ($response->successful()) {
//     //         $data = $response->json();
//     //         // Lakukan sesuatu dengan data yang diterima
//     //         return response()->json(['success' => 'Data Order Berhasil Ditambahkan!'], 200);
//     //     } else {
//     //         return response()->json(['error' => 'Gagal membuat data melalui API'], 500);
//     //     }
//     // }


//     // public function store(Request $request)
//     // {
//     //     // print "hello world"
//     //     $yourPublicToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI2MmNmZWY2NDQ4NGMxZTcwMTU0YjQ5YjIiLCJqdGkiOiIzY2ZmNDg2Y2ZlZThhMzZkMTkzMTJlZWQyY2FlNzRiNGNjMTMwZjQ5OTlkMjUzZGQ4MDk4NTBjODE0MTJmZDFhNDM2N2RhNTRiNDRlM2M4MiIsImlhdCI6MTY4MzcyNDE2MC45OTc4NDcsIm5iZiI6MTY4MzcyNDE2MC45OTc4NTEsImV4cCI6NDgzOTM5Nzc2MC45OTQ0MTYsInN1YiI6IjY0MzYxYTI3NjA1OGE1NjVhNDAwNTBjMSIsInNjb3BlcyI6W119.Z2Q-kw2g0IvpCe9U7CG5u7MZP5i9MKXfk-9T2BfxUm9oin7BaFO0CMoaCKJAF3Dq2XMn09BUeRPffJNp2m_eVq5wOGOJVtORjakvVPcb9Apen_I7OCLEcjGZQFonUCDYEHaWJVWjmuptxsk-NHzcWzRB70VC1RCu6ft1_LvAdTPEI-Kprb14_41mhtkPJtyQIbe_2AUyvCjOvTr3hCdFoeeWcB_S9sTBfteHAU1QaY7h2pDbnZ5mIvpzjmRaBP57WAKAQ4tlxsWAra8l6s9rh3yssKDdhTyepO9ehxCTjquCajyb2A-z97wIlIoSJJbwYcD-eCS8NLgnWXAiPe1Esg_cHC7OtWjkZcoUHO2zDzz_p2o0J89fFXi_7Bsluo2uKL5DGYwY39hoN5Lah-SqwSV6pNMPUCywGFZmrSwlm4N0s4xJo6prWTYwb9-VILoiE3IGXOJ1XOlPl9chOCM_-vR37a3qbGQiutjnOQia0dYanoW5QR750zIy5IxfEsvWBC5Pw_u1qnkdKEMkuqdST-_w5XNu999K1x0xcSuSlMb-tjWLawjuCKezG5RP2-ClVDbfBQfA-OthfV04lwUYqTy03vngtYEHReKPzSP-p6PAk3GzEughotLDSqIpfS3bhM9zWeraYwckyGCzVR1oHsE0_hdTKs5hQ6i10q5lVRQ';

//     //     $response = Http::withHeaders([
//     //         'Authorization' => 'Bearer '.$yourPublicToken,
//     //     ])->post('https://apiweb.mile.app/api/v3/task/', [
//     //         'data' => $request->all(),
//     //     ]);

//     //     if ($response->successful()) {
//     //         $data =  Validator::make($response->json());
//     //         // Lakukan sesuatu dengan data yang diterima
//     //         // return response()->json(['success' => 'Berhasil membuat data melalui API', 'data' => $data], 200);
//     //         return new OrderResource(true, 'Data Post Berhasil Ditambahkan!', $post);
//     //     } else {
//     //         return response()->json(['error' => 'Gagal membuat data melalui API'], 500);
//     //     }
//     // }


//     // public function store(StoreOrderRequest $request)
//     // {
//     //     //
//     // }

//     /**
//      * Display the specified resource.
//      */
//     public function show(Order $order)
//     {
//         //
//     }

//     // public function showTasks()
//     // {
//     //     $yourPublicToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI2MmNmZWY2NDQ4NGMxZTcwMTU0YjQ5YjIiLCJqdGkiOiJjODU2ZTIyYzA5ZWI4NGFjNmQxNTQwNzlhYzI5N2Q4OTYyMWUxODc3ZGU1MTA1MDFiZjdkYzA2NjY2Y2JiZTQwMzgxODg4M2RjZTU1MjY4OCIsImlhdCI6MTY4NDc3NDA3MSwibmJmIjoxNjg0Nzc0MDcxLCJleHAiOjQ4NDA0NDc2NzEsInN1YiI6IjY0NTliNDJlOWNhYWI2YTkyOTBjMjY5MiIsInNjb3BlcyI6W119.IdUhWYqieeNkFyF5EAcLQoaQZXtmDiqQiGZgSboBZlX5ccKhdsdK_9HPM8UH4Kfz3y4W-wr_at4HQZjV0Wt-PyVSaG-Xg810RKhnBmWPIzjPANoywvTJiRw6_7ujTxMIQcNcZMxkXB1tvA_T8I731_f1Vf1WBlgzNUfJ0TwySxZWIIY7y3021RUJAxxnVEGb3nBBJ-Y_tjQ2N2fS5mkmG4zSHbFqT_k-_-AmY-fsmMXHeXwxUONN3SP5k_jnnc9KGpSwmnXtGe4yIm38EHZIPOcRm9aalq2d5wiXHTem9VLwgWvLrjClBXfXYK96AeBhUuqTsPFSf-ZlGSlBikRdFUMLuADtWTEXBK1St64Zg3-Pu_9XeY_n8k1GsWYgMJgwOBl_ixuXd9DV86ovkNmzV2dS_5D2pI-MVsA2jgWuodSwfoldbY80dvX6CKWoFF25E5LFRmFlp3ZCE00zEu0aIpRH1jPinURbdxTwH2kRWG5Iq3dg8xv4B1x_6MNTraUS29fozqPLapoEe6U4lF5aVezXECc30x8ogHbys5r7Zgf_1-ikuUOKtoCQPnBD0H4dzvlbgkC0_ajTrSayfj3W-Nk6wwKN8bz0jPEOq7-mFTRY52dnyRzKIJsr1bUCDLAmvxuO8zhZxnWLvhpRDQPvEYoE_03mrscMKt4Ohw7Tp8k';

//     //     $response = Http::withHeaders([
//     //         'Authorization' => 'Bearer '.$yourPublicToken,
//     //     ])->get('https://apiweb.mile.app/api/v3/tasks');

//     //     if ($response->successful()) {
//     //         $data = $response->json();
//     //         return view('tasks')->with('tasks', $data);
//     //     } else {
//     //         return response()->json(['error' => 'Gagal mengambil data dari API'], 500);
//     //     }
//     // }

//     // public function showTasks()
//     // {
//     //     $yourPublicToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI2MmNmZWY2NDQ4NGMxZTcwMTU0YjQ5YjIiLCJqdGkiOiJjODU2ZTIyYzA5ZWI4NGFjNmQxNTQwNzlhYzI5N2Q4OTYyMWUxODc3ZGU1MTA1MDFiZjdkYzA2NjY2Y2JiZTQwMzgxODg4M2RjZTU1MjY4OCIsImlhdCI6MTY4NDc3NDA3MSwibmJmIjoxNjg0Nzc0MDcxLCJleHAiOjQ4NDA0NDc2NzEsInN1YiI6IjY0NTliNDJlOWNhYWI2YTkyOTBjMjY5MiIsInNjb3BlcyI6W119.IdUhWYqieeNkFyF5EAcLQoaQZXtmDiqQiGZgSboBZlX5ccKhdsdK_9HPM8UH4Kfz3y4W-wr_at4HQZjV0Wt-PyVSaG-Xg810RKhnBmWPIzjPANoywvTJiRw6_7ujTxMIQcNcZMxkXB1tvA_T8I731_f1Vf1WBlgzNUfJ0TwySxZWIIY7y3021RUJAxxnVEGb3nBBJ-Y_tjQ2N2fS5mkmG4zSHbFqT_k-_-AmY-fsmMXHeXwxUONN3SP5k_jnnc9KGpSwmnXtGe4yIm38EHZIPOcRm9aalq2d5wiXHTem9VLwgWvLrjClBXfXYK96AeBhUuqTsPFSf-ZlGSlBikRdFUMLuADtWTEXBK1St64Zg3-Pu_9XeY_n8k1GsWYgMJgwOBl_ixuXd9DV86ovkNmzV2dS_5D2pI-MVsA2jgWuodSwfoldbY80dvX6CKWoFF25E5LFRmFlp3ZCE00zEu0aIpRH1jPinURbdxTwH2kRWG5Iq3dg8xv4B1x_6MNTraUS29fozqPLapoEe6U4lF5aVezXECc30x8ogHbys5r7Zgf_1-ikuUOKtoCQPnBD0H4dzvlbgkC0_ajTrSayfj3W-Nk6wwKN8bz0jPEOq7-mFTRY52dnyRzKIJsr1bUCDLAmvxuO8zhZxnWLvhpRDQPvEYoE_03mrscMKt4Ohw7Tp8k';

//     //     $response = Http::withHeaders([
//     //         'Authorization' => 'Bearer '.$yourPublicToken,
//     //     ])->get('https://apiweb.mile.app/api/v3/tasks');

//     //     if ($response->successful()) {
//     //         $data = $response->json();
//     //         $arrayData = json_encode($data);
//     //         $a=json_decode($arrayData);

//     //         if (is_array($a)) { // Periksa apakah respons berupa array
//     //             return view('tasks')->with('tasks', $a);
//     //         } else {
//     //             return response()->json(['error' => 'Respon dari API tidak valid'], 500);
//     //         }
//     //     } else {
//     //         return response()->json(['error' => 'Gagal mengambil data dari API'], 500);
//     //     }
//     // }



//     /**
//      * Show the form for editing the specified resource.
//      */
//     public function edit(Order $order)
//     {
//         //
//     }

//     /**
//      * Update the specified resource in storage.
//      */
//     public function update(UpdateOrderRequest $request, Order $order)
//     {
//         //
//     }

//     /**
//      * Remove the specified resource from storage.
//      */
//     // public function destroy(Order $order)
//     // {
//     //     //
//     // }

//     public function destroy($taskId)
//     {
//         $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI2MmNmZWY2NDQ4NGMxZTcwMTU0YjQ5YjIiLCJqdGkiOiIzY2ZmNDg2Y2ZlZThhMzZkMTkzMTJlZWQyY2FlNzRiNGNjMTMwZjQ5OTlkMjUzZGQ4MDk4NTBjODE0MTJmZDFhNDM2N2RhNTRiNDRlM2M4MiIsImlhdCI6MTY4MzcyNDE2MC45OTc4NDcsIm5iZiI6MTY4MzcyNDE2MC45OTc4NTEsImV4cCI6NDgzOTM5Nzc2MC45OTQ0MTYsInN1YiI6IjY0MzYxYTI3NjA1OGE1NjVhNDAwNTBjMSIsInNjb3BlcyI6W119.Z2Q-kw2g0IvpCe9U7CG5u7MZP5i9MKXfk-9T2BfxUm9oin7BaFO0CMoaCKJAF3Dq2XMn09BUeRPffJNp2m_eVq5wOGOJVtORjakvVPcb9Apen_I7OCLEcjGZQFonUCDYEHaWJVWjmuptxsk-NHzcWzRB70VC1RCu6ft1_LvAdTPEI-Kprb14_41mhtkPJtyQIbe_2AUyvCjOvTr3hCdFoeeWcB_S9sTBfteHAU1QaY7h2pDbnZ5mIvpzjmRaBP57WAKAQ4tlxsWAra8l6s9rh3yssKDdhTyepO9ehxCTjquCajyb2A-z97wIlIoSJJbwYcD-eCS8NLgnWXAiPe1Esg_cHC7OtWjkZcoUHO2zDzz_p2o0J89fFXi_7Bsluo2uKL5DGYwY39hoN5Lah-SqwSV6pNMPUCywGFZmrSwlm4N0s4xJo6prWTYwb9-VILoiE3IGXOJ1XOlPl9chOCM_-vR37a3qbGQiutjnOQia0dYanoW5QR750zIy5IxfEsvWBC5Pw_u1qnkdKEMkuqdST-_w5XNu999K1x0xcSuSlMb-tjWLawjuCKezG5RP2-ClVDbfBQfA-OthfV04lwUYqTy03vngtYEHReKPzSP-p6PAk3GzEughotLDSqIpfS3bhM9zWeraYwckyGCzVR1oHsE0_hdTKs5hQ6i10q5lVRQ";
//         $apiUrl = "https://apiweb.mile.app/api/v3/task/{$taskId}";

//         try {
//             $client = new Client();
//             $response = $client->request('DELETE', $apiUrl, [
//                 'headers' => [
//                     'Authorization' => 'Bearer ' . $token,
//                     'Accept' => 'application/json',
//                 ],
//             ]);

//             $statusCode = $response->getStatusCode();
//             $responseData = $response->getBody()->getContents();

//             // Mengembalikan respons sesuai kebutuhan Anda
//             return response()->json([
//                 'status' => 'success',
//                 'code' => $statusCode,
//                 'data' => json_decode($responseData),
//             ]);

//         } catch (\Exception $e) {
//             // Mengembalikan respons jika terjadi kesalahan
//             return response()->json([
//                 'status' => 'error',
//                 'message' => $e->getMessage(),
//             ], 500);
//         }
//     }
// }

// //     /**
// //      * Daftar public API yang tersedia beserta tokennya.
// //      *
// //      * @var array
// //      */
// //     protected $apis = [
// //         'API 1' => [
// //             'url' => 'https://apiweb.mile.app/api/v3/tasks',
// //             'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI2MmNmZWY2NDQ4NGMxZTcwMTU0YjQ5YjIiLCJqdGkiOiJjODU2ZTIyYzA5ZWI4NGFjNmQxNTQwNzlhYzI5N2Q4OTYyMWUxODc3ZGU1MTA1MDFiZjdkYzA2NjY2Y2JiZTQwMzgxODg4M2RjZTU1MjY4OCIsImlhdCI6MTY4NDc3NDA3MSwibmJmIjoxNjg0Nzc0MDcxLCJleHAiOjQ4NDA0NDc2NzEsInN1YiI6IjY0NTliNDJlOWNhYWI2YTkyOTBjMjY5MiIsInNjb3BlcyI6W119.IdUhWYqieeNkFyF5EAcLQoaQZXtmDiqQiGZgSboBZlX5ccKhdsdK_9HPM8UH4Kfz3y4W-wr_at4HQZjV0Wt-PyVSaG-Xg810RKhnBmWPIzjPANoywvTJiRw6_7ujTxMIQcNcZMxkXB1tvA_T8I731_f1Vf1WBlgzNUfJ0TwySxZWIIY7y3021RUJAxxnVEGb3nBBJ-Y_tjQ2N2fS5mkmG4zSHbFqT_k-_-AmY-fsmMXHeXwxUONN3SP5k_jnnc9KGpSwmnXtGe4yIm38EHZIPOcRm9aalq2d5wiXHTem9VLwgWvLrjClBXfXYK96AeBhUuqTsPFSf-ZlGSlBikRdFUMLuADtWTEXBK1St64Zg3-Pu_9XeY_n8k1GsWYgMJgwOBl_ixuXd9DV86ovkNmzV2dS_5D2pI-MVsA2jgWuodSwfoldbY80dvX6CKWoFF25E5LFRmFlp3ZCE00zEu0aIpRH1jPinURbdxTwH2kRWG5Iq3dg8xv4B1x_6MNTraUS29fozqPLapoEe6U4lF5aVezXECc30x8ogHbys5r7Zgf_1-ikuUOKtoCQPnBD0H4dzvlbgkC0_ajTrSayfj3W-Nk6wwKN8bz0jPEOq7-mFTRY52dnyRzKIJsr1bUCDLAmvxuO8zhZxnWLvhpRDQPvEYoE_03mrscMKt4Ohw7Tp8k',
// //         ],
// //         'API 2' => [
// //             'url' => 'https://api.example.com/api2',
// //             'token' => 'YOUR_API2_TOKEN',
// //         ],
// //         // Tambahkan daftar public API lainnya beserta tokennya di sini
// //     ];

// //     /**
// //      * Mendapatkan daftar public API yang tersedia.
// //      *
// //      * @return \Illuminate\Http\JsonResponse
// //      */
// //     public function getAvailableApis()
// //     {
// //         return response()->json([
// //             'status' => 'success',
// //             'data' => $this->apis,
// //         ]);
// //     }

// //     /**
// //      * Mengirimkan request GET ke public API.
// //      *
// //      * @param  string  $apiName
// //      * @param  array  $params
// //      * @return \Illuminate\Http\JsonResponse
// //      */
// //     public function sendGetRequest($apiName, $params = [])
// //     {
// //         if (!isset($this->apis[$apiName])) {
// //             return response()->json([
// //                 'status' => 'error',
// //                 'message' => 'Public API not found.',
// //             ], 404);
// //         }

// //         $api = $this->apis[$apiName];
// //         $apiUrl = $api['url'];
// //         $token = $api['token'];

// //         try {
// //             $client = new Client();
// //             $response = $client->request('GET', $apiUrl, [
// //                 'query' => $params,
// //                 'headers' => [
// //                     'Authorization' => 'Bearer ' . $token,
// //                 ],
// //             ]);

// //             $statusCode = $response->getStatusCode();
// //             $responseData = $response->getBody()->getContents();

// //             return response()->json([
// //                 'status' => 'success',
// //                 'code' => $statusCode,
// //                 'data' => json_decode($responseData),
// //             ]);

// //         } catch (\Exception $e) {
// //             return response()->json([
// //                 'status' => 'error',
// //                 'message' => $e->getMessage(),
// //             ], 500);
// //         }
// //     }

// //     /**
// //      * Mengirimkan request POST ke public API.
// //      *
// //      * @param  string  $apiName
// //      * @param  array  $data
// //      * @return \Illuminate\Http\JsonResponse
// //      */
// //     public function sendPostRequest($apiName, $data = [])
// //     {
// //         if (!isset($this->apis[$apiName])) {
// //             return response()->json([
// //                 'status' => 'error',
// //                 'message' => 'Public API not found.',
// //             ], 404);
// //         }

// //         $api = $this->apis[$apiName];
// //         $apiUrl = $api['url'];
// //         $token = $api['token'];

// //         try {
// //             $client = new Client();
// //             $response = $client->request('POST', $apiUrl, [
// //                 'json' => $data,
// //                 'headers' => [
// //                     'Authorization' => 'Bearer ' . $token,
// //                 ],
// //             ]);

// //             $statusCode = $response->getStatusCode();
// //             $responseData = $response->getBody()->getContents();

// //             return response()->json([
// //                 'status' => 'success',
// //                 'code' => $statusCode,
// //                 'data' => json_decode($responseData),
// //             ]);

// //         } catch (\Exception $e) {
// //             return response()->json([
// //                 'status' => 'error',
// //                 'message' => $e->getMessage(),
// //             ], 500);
// //         }
// //     }

// //     /**
// //      * Mengirimkan request DELETE ke public API.
// //      *
// //      * @param  string  $apiName
// //      * @return \Illuminate\Http\JsonResponse
// //      */
// //     public function sendDeleteRequest($apiName)
// //     {
// //         if (!isset($this->apis[$apiName])) {
// //             return response()->json([
// //                 'status' => 'error',
// //                 'message' => 'Public API not found.',
// //             ], 404);
// //         }

// //         $api = $this->apis[$apiName];
// //         $apiUrl = $api['url'];
// //         $token = $api['token'];

// //         try {
// //             $client = new Client();
// //             $response = $client->request('DELETE', $apiUrl, [
// //                 'headers' => [
// //                     'Authorization' => 'Bearer ' . $token,
// //                 ],
// //             ]);

// //             $statusCode = $response->getStatusCode();
// //             $responseData = $response->getBody()->getContents();

// //             return response()->json([
// //                 'status' => 'success',
// //                 'code' => $statusCode,
// //                 'data' => json_decode($responseData),
// //             ]);

// //         } catch (\Exception $e) {
// //             return response()->json([
// //                 'status' => 'error',
// //                 'message' => $e->getMessage(),
// //             ], 500);
// //         }
// //     }
// // }

