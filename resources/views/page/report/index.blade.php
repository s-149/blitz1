 <?php $a =1 ; ?>

 @extends('layout.main')

 @section('container')

                <section id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="col-lg-3">
                                    <a href="/fakturstore" class="btn btn-primary">Create</a>
                                </div>
                                <br>
                                <div class="bootstrap-data-table-panel">
                                    <div class="table-responsive">
                                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Faktur</th>
                                                    <th>Status</th>
                                                    <th>Hub</th>
                                                    <th>Y</th>
                                                    <th>M</th>
                                                    <th>D</th>
                                                    <th>Time</th>
                                                    <th>Sla</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($faktur as $faktur)

                                                <tr>
                                                    <td>{{ $a++ }}</td>
                                                    <td>{{ $faktur->faktur }}</td>
                                                    <td>{{ $faktur->status }}</td>
                                                    <td>{{ $faktur->hub }}</td>
                                                    <td>{{ $faktur->y }}</td>
                                                    <td>{{ $faktur->m }}</td>
                                                    <td>{{ $faktur->d }}</td>
                                                    <td>{{ $faktur->time }}</td>
                                                    <td>{{ $faktur->sla }}</td>
                                                </tr>

                                                @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

@endsection
