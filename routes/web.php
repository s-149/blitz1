<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Order;
use App\Http\Controllers\OrderController;
use App\Models\Task;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\ReportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/task', [TaskController::class, 'index']);
Route::get('/taskstore', [TaskController::class, 'create']);
Route::post('/taskstore', [TaskController::class, 'store']);
Route::get('/taskdelete{id}', [TaskController::class, 'destroy']);

Route::get('/masterReport', [ReportController::class, 'index']);
Route::get('/hubReport', [ReportController::class, 'hubReport']);
Route::get('/dailyReport', [ReportController::class, 'dailyReport']);

Route::get('/test', [ReportController::class, 'test' ] );

// Route::get('/siswashow{id}', [SiswaController::class, 'show'])->middleware('auth');


// curl --location 'https://apiweb.mile.app/api/v3/task'                                 --header 'Access-Control-Allow-Origin: *'                                 --header 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI2MmNmZWY2NDQ4NGMxZTcwMTU0YjQ5YjIiLCJqdGkiOiJjZjM4MTkzZmY2ODkxNjhlN2NkMGE1NmYyZTY5NzMxOWJiY2Y0ODIyYjgzMmU4MjgwYjEwZmE2YmE4NzlmZDFkZjQxY2Y4N2NlNTI3MDIxOSIsImlhdCI6MTY4NDgzNTYyMiwibmJmIjoxNjg0ODM1NjIyLCJleHAiOjE3MTY0NTgwMjIsInN1YiI6IjY0NTliNDJlOWNhYWI2YTkyOTBjMjY5MiIsInNjb3BlcyI6W119.gWGYaH6wT83V9UA1Soe9sKtkCP6h7f25pnC64QipxMI7bJJRr6S8p_GjInj7xktqwLGgjbJ5TnGtpMx6CArEV6rWyT9yzliUgEueDudNW3PLwWvRKJT__YdLFwHUwEP0MK99mRSJY2djQyfTUX07M9K39CNRgWqP0g14_NykpFYP-u5DrkpaA45bD-AY1TNX7ZbAl7hsMk0EwdWdjWm69_nUbCXTS0f0wh7009_f69j3YtQ0fpwxv2hTSB0jVhv9n9a_zi8cGROrSV8qHyUJMvdO_H3Xim4z185y1j8zzNUYQEdA5HOEc7C2TJvDoBTPBis7BUCZeVwOOx4k3RV5wRhs6HTExBXwBNECYj5hglKG_GBYCgwZCiKJyHhtDgy6hbJTGOfSxldVIpHmRsAHXZODdiP5nh108ntncyTzBWgWs694UxYrsHJUyf266gxtpx-QgV-qS4KS0ad3qTSoPXzWx-cCvXELyyCq2zEprivqL5Kz3GkD7s5rzuZd6drOB82ErfgJDYoUMR5k-lV5devq3Mh4giy7kLRfDLhzHN3_H15poriGyqR7DtENVsoXZhrq8M2HnfL26an7PfQXaEUWYUsxqKXHTh3uT3fdrlz71YI1bu5zOTx0fPI6kZZHbIzRNvOnuVWes-1yZ8rZ_snZFSVoqGEDyw2cjEAlfiM'                                 --header 'Connection: keep-alive'                                 --header 'Content-Type: application/json'                                 --header 'Cookie: laravel_session=l8hpqmfwH7Vg722rzuVf0SNgp6gp1Z649bbGiyni; laravel_session=l8hpqmfwH7Vg722rzuVf0SNgp6gp1Z649bbGiyni'                                 --data-raw '{
//     "hubId": "64361bce5c53b4c3c0039398",
//     "flow": "Trial BLITZ",
//     "startTime": "2023-05-24T17:09:21.000+00:00",
//     "endTime": "2023-05-25T17:09:21.000+00:00",
//     "namaKlien": "vel",
//     "alamat": "917 Marquis Road Apt. 487\nLeuschketon, NC 48894-1355",
//     "koordinat": "52.590629,-92.028421",
//     "hampersType": "adipisci",
//     "telefon": "081247627739"
//   }'
