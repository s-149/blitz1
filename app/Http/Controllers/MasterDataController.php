<?php

namespace App\Http\Controllers;

use App\Models\Master_Data;
use App\Http\Requests\StoreMaster_DataRequest;
use App\Http\Requests\UpdateMaster_DataRequest;

class MasterDataController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreMaster_DataRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Master_Data $master_Data)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Master_Data $master_Data)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateMaster_DataRequest $request, Master_Data $master_Data)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Master_Data $master_Data)
    {
        //
    }
}
