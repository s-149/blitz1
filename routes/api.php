<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Order;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\TaskController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/tasks', [TaskController::class, 'getAllTask']);
Route::post('/task', [TaskController::class, 'postTask']);
// Route::post('order/{taskId}', [UserController::class, 'destroy']);


// Route::get('/task', [TaskController::class, 'sendGetRequest']);
// Route::apiResource('/order', App\Http\Controllers\OrderController::class);

