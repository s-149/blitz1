 <?php

$noData =1 ;

 ?>

 @extends('layout.main')

 @section('container')

                <section id="main-content">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="bootstrap-data-table-panel">
                                    <div class="table-responsive">
                                        <table id="bootstrap-data-table-export2" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Tanggal</th>
                                                    <th>Jumlah Late</th>
                                                    <th>Jumlah On Time</th>
                                                    <th>Jumlah Faktur</th>
                                                    <th>Percent Late</th>
                                                    <th>Percent On Time</th>

                                                </tr>
                                            </thead>
                                            <tbody>

                                                @foreach ($days as $days)

                                                <tr>
                                                    <td>
                                                        {{ $noData ++ }}
                                                    </td>
                                                    <td>
                                                        {{ $days['tanggal']; }}
                                                    </td>
                                                    <td>
                                                        {{ count($days['late']); }}
                                                    </td>
                                                    <td>
                                                        {{ count($days['onTime']); }}
                                                    </td>
                                                    <td>
                                                        {{ count($days['late']) + count($days['onTime']); }}
                                                    </td>
                                                    <td>@if(count($days['late']) >= 1)
                                                        {{ round((count($days['late']) / (count($days['late']) + count($days['onTime']))) * 100 , 2); }}

                                                        @else
                                                        {{ 0 }}

                                                        @endif
                                                    </td>
                                                    <td>@if(count($days['onTime']) >= 1)
                                                        {{ round((count($days['onTime']) / (count($days['late']) + count($days['onTime']))) * 100 , 2); }}

                                                        @else
                                                        {{ 0 }}

                                                        @endif
                                                    </td>

                                                </tr>

                                                @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

@endsection
