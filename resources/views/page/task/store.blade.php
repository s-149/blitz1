@extends('layout.main')

@section('container')

				<section id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-validation">
                                        <form class="form-valide" action="/taskstore" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="flow">Flow</label>
                                                <div class="col-lg-8">
                                                    <input type="text" class="form-control" id="flow" name="flow" placeholder=" Flow.." value="Trial BLITZ" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="hubId">Hub Id</label>
                                                <div class="col-lg-8">
                                                    <input type="text" class="form-control" id="hubId" name="hubId" placeholder=" Hub Id.." value="64361bce5c53b4c3c0039398" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="startTime">Start Time</label>
                                                <div class="col-lg-8">
                                                    <input type="text" class="form-control" id="startTime" name="startTime" placeholder=" Start Time.." value="2023-05-22T10:37:00.000+07:00" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="endTime">End Time</label>
                                                <div class="col-lg-8">
                                                    <input type="text" class="form-control" id="endTime" name="endTime" placeholder="End Time.." value="2023-05-23T10:37:00.000+07:00" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="createdFrom">Created From</label>
                                                <div class="col-lg-8">
                                                    <input type="text" class="form-control" id="createdFrom" name="createdFrom" placeholder=" Created From.." value="WEB" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="namaKlien">Nama Klien</label>
                                                <div class="col-lg-8">
                                                    <input type="text" class="form-control" id="namaKlien" name="namaKlien" placeholder=" Nama Klien.." value="Sabar Test" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="alamat">Alamat</label>
                                                <div class="col-lg-8">
                                                    <textarea name="alamat" id="alamat" cols="30" rows="10" placeholder="Alamat">Taman Meruya Ilir A13 No 15, Meruya Utara, Kembangan, Jakarta Barat</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="koordinat">Koordinat</label>
                                                <div class="col-lg-8">
                                                    <input type="text" class="form-control" id="koordinat" name="koordinat" placeholder=" Jam Persalinan.." value="-6.19194,106.73969" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="hampersType">Hampers Type</label>
                                                <div class="col-lg-8">
                                                    <input type="text" class="form-control" id="hampersType" name="hampersType" placeholder=" Hampers Type.." value="tes,,, Input Baru Lagi ya,, Buat Contoh saja lagiii...." required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="telefon">No Telp</label>
                                                <div class="col-lg-8">
                                                    <input type="number" class="form-control" id="telefon" name="telefon" placeholder=" No Telp.." value="08158224448" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-8 ml-auto">
                                                    <button type="submit" class="btn btn-primary">Submit</button> or
                                                    <a class="btn btn-danger" href="/task">Cancel</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

@endsection
