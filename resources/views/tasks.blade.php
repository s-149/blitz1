<!DOCTYPE html>
<html>
<head>
    <title>Tasks</title>
</head>
<body>
    <h1>Tasks</h1>

    @if (isset($tasks))
        <ul>
            @foreach ($tasks as $task)
                <li>{{ $task['name'] }}</li>
            @endforeach
        </ul>
    @else
        <p>Tidak ada data task.</p>
    @endif
</body>
</html>
