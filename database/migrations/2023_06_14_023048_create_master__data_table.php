<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('master__data', function (Blueprint $table) {
            $table->id();

            // $table->string('_id');
            // $table->string('flow');
            // $table->string('hubId');
            // $table->string('hub');
            // $table->string('startTime');
            // $table->string('endTime');
            // $table->string('createdFrom');
            // $table->string('title');
            // $table->string('content');
            // $table->string('label');

            // $table->string('organizationId');
            // $table->string('status');
            // $table->string('orderIndex');
            // $table->string('createdBy');
            // $table->string('updatedBy');
            // $table->string('travelDuration');
            // $table->string('doneTime');
            // $table->string('doneBy');
            // $table->string('assignedBy');
            // $table->string('assignedTime');

            // $table->string('assignedTo');
            // $table->string('assignee');
            // $table->string('createdCoordinate');
            // $table->string('doneCoordinate');
            // $table->string('createdTime');
            // $table->string('updatedTime');
            // $table->string('NomorInvoice');
            // $table->string('NamaCustomer');
            // $table->string('AlamatCustomer');
            // $table->string('Catatan');

            // $table->string('COD');
            // $table->string('BuktiFotoPengiriman');
            // $table->string('buktiFotoPengiriman_0_url');
            // $table->string('Order_number');
            // $table->string('Customer_name');
            // $table->string('Total_package');
            // $table->string('Volume');
            // $table->string('Volume_unit');
            // $table->string('Width');
            // $table->string('Height');

            // $table->string('Length');
            // $table->string('Weight');
            // $table->string('Service_type');
            // $table->string('Service_ride_type');
            // $table->string('Service_promo_code');
            // $table->string('Service_payment_type');
            // $table->string('Service_cod_amount');
            // $table->string('Service_total_amount');
            // $table->string('Sender_name');
            // $table->string('Sender_phone');

            // $table->string('Sender_district');
            // $table->string('Sender_city');
            // $table->string('Sender_province');
            // $table->string('Sender_postalcode');
            // $table->string('Sender_country');
            // $table->string('Sender_longlat');
            // $table->string('Sender_slot_start');
            // $table->string('Sender_slot_end');
            // $table->string('Sender_email');
            // $table->string('Sender_address');

            // $table->string('Sender_note');
            // $table->string('Consignee_name');
            // $table->string('Consignee_phone');
            // $table->string('Consignee_email');
            // $table->string('Consignee_district');
            // $table->string('Consignee_city');
            // $table->string('Consignee_province');
            // $table->string('Consignee_postalcode');
            // $table->string('Consignee_longlat');
            // $table->string('Consignee_slot_start');

            // $table->string('Consignee_slot_end');
            // $table->string('Consignee_address');
            // $table->string('Consignee_country');
            // $table->string('Consignee_note');
            // $table->string('Description');
            // $table->string('Product_id');
            // $table->string('Product_name');
            // $table->string('OrderNumber');
            // $table->string('CustomerName');
            // $table->string('Address');

            // $table->string('Coordinate');
            // $table->string('PhoneNumber');
            // $table->string('VisitDate&Time');
            // $table->string('ServiceSurveyType');
            // $table->string('BuildingSurveyType');
            // $table->string('VisitDate&Time');
            // $table->string('Buildingarea(m2)');
            // $table->string('NumberofFloors');
            // $table->string('AreaCleaning');
            // $table->string('BuildingConditions');

            // $table->string('CurrentBuildingConditions');
            // $table->string('currentBuildingConditions_0_url');
            // $table->string('currentBuildingConditions_1_url');
            // $table->string('currentBuildingConditions_2_url');
            // $table->string('currentBuildingConditions_3_url');
            // $table->string('currentBuildingConditions_4_url');
            // $table->string('currentBuildingConditions_5_url');
            // $table->string('currentBuildingConditions_6_url');
            // $table->string('currentBuildingConditions_7_url');
            // $table->string('currentBuildingConditions_8_url');

            // $table->string('currentBuildingConditions_9_url');
            // $table->string('currentBuildingConditions_10_url');
            // $table->string('currentBuildingConditions_11_url');
            // $table->string('currentBuildingConditions_12_url');
            // $table->string('currentBuildingConditions_13_url');
            // $table->string('currentBuildingConditions_14_url');
            // $table->string('currentBuildingConditions_15_url');
            // $table->string('currentBuildingConditions_16_url');
            // $table->string('currentBuildingConditions_17_url');
            // $table->string('currentBuildingConditions_18_url');

            // $table->string('currentBuildingConditions_19_url');
            // $table->string('WorkTime/DateProcess');
            // $table->string('ProcessingTime');
            // $table->string('TotalWorkers');
            // $table->string('ToolsUsed');
            // $table->string('Signature');
            // $table->string('signature_url');
            // $table->string('Note(Optional)');
            // $table->string('OrderNumber');
            // $table->string('StoreName');

            // $table->string('OwnerName');
            // $table->string('Address');
            // $table->string('Coordinate');
            // $table->string('Phone');
            // $table->string('VisitDate&Time');
            // $table->string('List');
            // $table->string('CustomerType');
            // $table->string('ProductType');
            // $table->string('StatusCanvassing');
            // $table->string('List');

            // $table->string('PaymentType');
            // $table->string('PaymentMethod');
            // $table->string('ProofonCanvassing');
            // $table->string('proofOnCanvassing_0_url');
            // $table->string('proofOnCanvassing_1_url');
            // $table->string('proofOnCanvassing_2_url');
            // $table->string('Signature');
            // $table->string('signature_url');
            // $table->string('Note(Optional)');
            // $table->string('OrderNumber');

            // $table->string('StoreName');
            // $table->string('OwnerName');
            // $table->string('Address');
            // $table->string('Coordinate');
            // $table->string('Date&Time');
            // $table->string('Phone');
            // $table->string('ProductBill');
            // $table->string('productBill.Discount');
            // $table->string('productBill.ShippingCost');
            // $table->string('productBill.totalCost');

            // $table->string('SalesStatus');
            // $table->string('ProductBill');
            // $table->string('productBill-1.Discount');
            // $table->string('productBill-1.ShippingCost');
            // $table->string('productBill-1.totalCost');
            // $table->string('PaymentType');
            // $table->string('PaymentMethod');
            // $table->string('Proofofsales');
            // $table->string('proofOfSales_0_url');
            // $table->string('proofOfSales_1_url');

            // $table->string('proofOfSales_2_url');
            // $table->string('Signature');
            // $table->string('signature_url');
            // $table->string('Note(Optional)');
            // $table->string('OrderNumber');
            // $table->string('OwnerName');
            // $table->string('Address');
            // $table->string('Coordinate');
            // $table->string('ServiceCategory');
            // $table->string('AdditionalService');

            // $table->string('Other');
            // $table->string('RoomPhoto(after)');
            // $table->string('roomPhotoAfter_0_url');
            // $table->string('roomPhotoAfter_1_url');
            // $table->string('roomPhotoAfter_2_url');
            // $table->string('roomPhotoAfter_3_url');
            // $table->string('roomPhotoAfter_4_url');
            // $table->string('roomPhotoAfter_5_url');
            // $table->string('roomPhotoAfter_6_url');
            // $table->string('roomPhotoAfter_7_url');

            // $table->string('roomPhotoAfter_8_url');
            // $table->string('signature1');
            // $table->string('signature1_url');
            // $table->string('Faktur');
            // $table->string('Cabang');
            // $table->string('Nama');
            // $table->string('NomorTelepon');
            // $table->string('Alamat');
            // $table->string('Jenis');
            // $table->string('Kordinat');

            // $table->string('BuktiFotoPengiriman');
            // $table->string('buktiFotoPengiriman_0_url');
            // $table->string('NamaKlien');
            // $table->string('Alamat');
            // $table->string('Koordinat');
            // $table->string('HampersType');
            // $table->string('Telefon');
            // $table->string('Buktifotopengiriman');
            // $table->string('buktiFotoPengiriman_0_url');
            // $table->string('buktiFotoPengiriman_1_url');

            // $table->string('PickupNumber');
            // $table->string('RequesterName');
            // $table->string('RequesterPhone');
            // $table->string('Address');
            // $table->string('PickupPoint');
            // $table->string('PackageType');
            // $table->string('PickupPicture');
            // $table->string('pickupPicture_0_url');
            // $table->string('pickupPicture_1_url');
            // $table->string('pickupPicture_2_url');

            // $table->string('Signature');
            // $table->string('signature1_url');
            // $table->string('Invoice');
            // $table->string('CustomerName');
            // $table->string('Address');
            // $table->string('Phone');
            // $table->string('Waybill');
            // $table->string('PaymentType');
            // $table->string('DeliveryCoordinate');
            // $table->string('Pricing');

            // $table->string('pricing.DeliveryFee');
            // $table->string('pricing.Discount');
            // $table->string('pricing.CODPrice');
            // $table->string('pricing.totalCost');
            // $table->string('ConfirmPayment');
            // $table->string('DeliveryStatus');
            // $table->string('Reason(ifFailed)');
            // $table->string('OrderPrice');
            // $table->string('orderPrice.DeliveryFee');
            // $table->string('orderPrice.CODPrice');

            // $table->string('orderPrice.Discount');
            // $table->string('orderPrice.totalCost');
            // $table->string('ReceiverName');
            // $table->string('ProofOfDelivery(photo)');
            // $table->string('proofOfDeliveryPhoto_0_url');
            // $table->string('proofOfDeliveryPhoto_1_url');
            // $table->string('proofOfDeliveryPhoto_2_url');
            // $table->string('Signature');
            // $table->string('signature1_url');



            $table->string('_id');
            $table->string('flow');
            $table->string('hubId');
            $table->string('hub');
            $table->string('startTime');
            $table->string('endTime');
            $table->string('createdFrom');
            $table->string('title');
            $table->string('content');
            $table->string('label');

            $table->string('organizationId');
            $table->string('status');
            $table->string('orderIndex');
            $table->string('createdBy');
            $table->string('updatedBy');
            $table->string('travelDuration');
            $table->string('doneTime');
            $table->string('doneBy');
            $table->string('assignedBy');
            $table->string('assignedTime');

            $table->string('assignedTo');
            $table->string('assignee');
            $table->string('createdCoordinate');
            $table->string('doneCoordinate');
            $table->string('createdTime');
            $table->string('updatedTime');
            $table->string('NomorInvoice');
            $table->string('NamaCustomer');
            $table->string('AlamatCustomer');
            $table->string('Catatan');

            $table->string('COD');
            $table->string('BuktiFotoPengiriman');
            $table->string('buktiFotoPengiriman_0_url');

            $table->string('Note(Optional)');
            $table->string('OrderNumber');
            $table->string('StoreName');
            $table->string('OwnerName');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('master__data');
    }
};
